This is a demo for infiniteScroll directive.

INSTALLATION:
Copy the infiniteScroll.js file to your application and load it (like I did in index.html -> <script src="scripts/directives/infiniteScroll.js"></script>).

USAGE:
1. add 'infiniteScroll' module wherever you want to use it. In our case, having a small application, wouldn't worth making a separate module for the main controller, and then add the infiniteScroll module there. So we added it in the app.js, therefore it's accesible in the entire application.

Example: in app.js, we added the module 'infiniteScroll' as a dependency to the project module.

angular
  .module('infiniteScrollApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'infiniteScroll'
  ])


2. Make sure the tag you want to make it infinite scrollable, is scrollable. 
NOTE: even though on UI, it may look that the container is scrolling, this might fool you, and be sure it's not the parent who's scrolling. How to do that? Set a height and overflow-y:hidden on each parent of the container. Then set the container a height and make overflow-y:auto;

After you made sure the container is scrollable, add the attribute 'infinite-scroll', and set its value with the name of the method from the controller's scope which will take care of loading more data. And this is pretty much it. Simple, right?

Example:
<div class='container' infinite-scroll='onInfiniteScroll()'> // onInfiniteScroll() will load more photos in $scope.photos variable
	<img ng-repeat="photo in photos" ng-src="{{photo}}"></div>
</div>

3. Also, if you want your scroller to be more sensitive, like scrolling before your scroll reaches the bottom of the container, you can do that by setting the attribute 'sensitivity' to the same container, with values within 0 and 100, meaning the percentage of the container's height that the scroll must to go down so that the loading method triggers.

Example:
<div class='container' infinite-scroll='onInfiniteScroll()' sensitivity='60'>
	<img ng-repeat="photo in photos" ng-src="{{photo}}"></div>
</div>
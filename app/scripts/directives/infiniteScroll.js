'use strict';
angular
  .module('infiniteScroll', [])
  .directive('infiniteScroll', ['$timeout', function($timeout) {
    return {
      scope: {
        sensitivity: '@',
        infiniteScroll: '&'
      },
      link: function(scope, elm) {
        if (scope.sensitivity && !isNaN(scope.sensitivity)) {
          scope.sensitivity = parseInt(scope.sensitivity);
          scope.sensitivity = (scope.sensitivity > 100) ? 100 : scope.sensitivity;
          scope.sensitivity = (scope.sensitivity < 0) ? 0 : scope.sensitivity;
        } else {
          scope.sensitivity = 0;
        }
        var raw = elm[0],
          distanceTillBottomWhenToTrigger = ((100 - scope.sensitivity) / 100) * raw.scrollHeight;

        elm.bind('scroll', function() {
          if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight - distanceTillBottomWhenToTrigger) {
            $timeout(function() {
              scope.infiniteScroll();
            });
          }
        });
      }
    };
  }]);



//   .directive('infiniteScroll', function() {
//     return function(scope, elm, attr) {
//         var raw = elm[0];

//         elm.bind('scroll', function() {
//             if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
//                 scope.$apply(attr.infiniteScroll);
//             }
//         });
//     };
// });

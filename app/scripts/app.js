'use strict';

/**
 * @ngdoc overview
 * @name infiniteScrollApp
 * @description
 * # infiniteScrollApp
 *
 * Main module of the application.
 */
angular
  .module('infiniteScrollApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'infiniteScroll',
    'jkuri.gallery'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

'use strict';

function mainService($timeout) {
  var _nrPhotos = 34;
  function _getMorePhotos(offset, length, callback) {
    var toReturn = [],
        i;
    for (i = offset; (i < length + offset); i++) {
      if (i <= _nrPhotos) {
        // only 34 photos are saved in /images
        toReturn.push('/images/' + i + '.jpg');
      } else {
        break;
      }
    }
    $timeout(function() {
      // simulate hard work. send answer after 2 seconds
      if (callback) {
        callback(toReturn);
      }
    }, 2000);
  }

  return {
    getMorePhotos: _getMorePhotos
  };
}

mainService.$inject = ['$timeout'];
angular
  .module('infiniteScrollApp')
  .factory('mainService', mainService);

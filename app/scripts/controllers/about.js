'use strict';

/**
 * @ngdoc function
 * @name infiniteScrollApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the infiniteScrollApp
 */
angular.module('infiniteScrollApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

function mainCtrl($scope, $document, mainService) {

  $scope.photos = [];
  $scope.loading = false;
  $scope.parameter = {};
  $scope.onInfiniteScroll = function() {
    if ($scope.loading || $scope.allPhotosReturned) {
      console.log('Work already in progress, or all photos have been received. Aborting...');
      return;
    }
    $scope.loading = true;
    mainService.getMorePhotos($scope.photos.length + 1, numberOfPhotosToRetrieve, function(results) {
      if (results.length < 5) {
        $scope.allPhotosReturned = true;
      }
      $scope.photos = $scope.photos.concat(results);
      _setImagesForGallery();
      $scope.loading = false;
    });
  };

  var numberOfPhotosToRetrieve = 10;

  function _setImagesForGallery() {
    var images = [],
      currentImage;
    for (var i = 0; i < $scope.photos.length; i++) {
      currentImage = {
        thumb: $scope.photos[i],
        img: $scope.photos[i]
      };
      images.push(currentImage);
    }
    $scope.images = images;
  }

  function init() {
    if ($scope.loading || $scope.allPhotosReturned) {
      console.log('Work already in progress, or all photos have been received. Aborting...');
      return;
    }
    $scope.loading = true;
    mainService.getMorePhotos(1, numberOfPhotosToRetrieve, function(results) {
      if (results.length < numberOfPhotosToRetrieve) {
        $scope.allPhotosReturned = true;
      }
      $scope.photos = results;
      _setImagesForGallery();
      $scope.loading = false;
    });
  }

  init();
}

mainCtrl.$inject = ['$scope', '$document', 'mainService'];

angular
  .module('infiniteScrollApp')
  .controller('MainCtrl', mainCtrl);
